import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Mid7wonderPage } from './mid7wonder';

@NgModule({
  declarations: [
    Mid7wonderPage,
  ],
  imports: [
    IonicPageModule.forChild(Mid7wonderPage),
  ],
})
export class Mid7wonderPageModule {}
