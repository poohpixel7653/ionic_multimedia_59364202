import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { New7wonderPage } from './new7wonder';

@NgModule({
  declarations: [
    New7wonderPage,
  ],
  imports: [
    IonicPageModule.forChild(New7wonderPage),
  ],
})
export class New7wonderPageModule {}
