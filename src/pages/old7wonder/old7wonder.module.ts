import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Old7wonderPage } from './old7wonder';

@NgModule({
  declarations: [
    Old7wonderPage,
  ],
  imports: [
    IonicPageModule.forChild(Old7wonderPage),
  ],
})
export class Old7wonderPageModule {}
