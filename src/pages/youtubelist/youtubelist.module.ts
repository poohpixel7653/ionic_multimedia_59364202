import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { YoutubelistPage } from './youtubelist';

@NgModule({
  declarations: [
    YoutubelistPage,
  ],
  imports: [
    IonicPageModule.forChild(YoutubelistPage),
  ],
})
export class YoutubelistPageModule {}
