import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { New7wonderPage } from '../new7wonder/new7wonder';
import { TabsService } from '../../shared/Service/tabs.service';
import { Mid7wonderPage } from '../mid7wonder/mid7wonder';
import { Old7wonderPage } from '../old7wonder/old7wonder';

/**
 * Generated class for the ProjecthomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-projecthome',
  templateUrl: 'projecthome.html',
  
})

export class ProjecthomePage {
  imgData: any;
  autoplay: any;
  constructor(public tabsService : TabsService,public navCtrl: NavController, public navParams: NavParams) {
    this.imgData = [{
      'src' : 'assets/imgs/7wonder_mid/Colosseum.jpg'
    }, {
      'src' : 'assets/imgs/7wonder_mid/Hagia-Sophia-01.jpg'
    }, {
      'src' : 'assets/imgs/7wonder_mid/Leaning-Tower-of-Pisa.jpg'
    },{
      'src' : 'assets/imgs/7wonder_mid/Stonehenge-02.jpg'
    },{
      'src' : 'assets/imgs/7wonder_new/Cristo_Redentor.jpg'
    },{
      'src' : 'assets/imgs/7wonder_new/Machu_Picchu.jpg'
    },{
      'src' : 'assets/imgs/7wonder_new/Petra.jpg'
    },{
      'src' : 'assets/imgs/7wonder_new/Taj_Mahal.jpg'
    }
  ];
  this.autoplay = 3000;
  }

  viewDetailNew7Wonder() {
    this.navCtrl.push(New7wonderPage);
  }

  viewDetailMid7Wonder(){
    this.navCtrl.push(Mid7wonderPage);
  }

  viewDetailOld7Wonder(){
    this.navCtrl.push(Old7wonderPage);
  }
  ionViewDidLoad(){
    this.tabsService.hide();
  }
}
