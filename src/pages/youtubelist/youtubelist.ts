import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
/**
 * Generated class for the YoutubelistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-youtubelist',
  templateUrl: 'youtubelist.html',
})
export class YoutubelistPage {
  channelId: string ='UC9MAhZQQd9egwWCxrwSIsJQ';
  maxRes: string ='10';
  googleToken: string ='AIzaSyDrB8weCwOW2H7y965dZXLE8Ext-Raon0g';
  search: string = 'history,historian';
  posts:any=[];

  constructor(public http : Http,public navCtrl: NavController, public navParams: NavParams) {
    let url="https://www.googleapis.com/youtube/v3/search?part=id,snippet&channelId=" 
    + this.channelId + "&q=" + this.search + "&type=video&order=date&maxResults=" + 
    this.maxRes + "&key=" + this.googleToken;
    this.http.get(url).map(res => res.json()).subscribe(data => {
      this.posts=this.posts.concat(data.items);
      console.log(this.posts);
    })
  }

  

}
