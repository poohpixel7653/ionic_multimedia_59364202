import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProjecthomePage } from './projecthome';

@NgModule({
  declarations: [
    ProjecthomePage,
  ],
  imports: [
    IonicPageModule.forChild(ProjecthomePage),
  ],
})
export class ProjecthomePageModule {}
