import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GoogleCloudVisionServiceProvider } from '../providers/google-cloud-vision-service/google-cloud-vision-service';

import { HttpModule } from '@angular/http';
import { environment } from '../environment';
import { Camera } from '@ionic-native/camera';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { TabsService } from '../shared/Service/tabs.service';
import { ProjecthomePage } from '../pages/projecthome/projecthome';
import { New7wonderPage } from '../pages/new7wonder/new7wonder';
import { Old7wonderPage } from '../pages/old7wonder/old7wonder';
import { Mid7wonderPage } from '../pages/mid7wonder/mid7wonder';
import { YoutubelistPage } from '../pages/youtubelist/youtubelist';
import { YoutubePipe } from '../pipes/youtube/youtube';


import * as ionicGalleryModal from 'ionic-gallery-modal';
import { HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { ModalPage } from '../pages/modal/modal';
@NgModule({

  declarations: [
    MyApp,ModalPage,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    ProjecthomePage,
    New7wonderPage,
    Mid7wonderPage,
    Old7wonderPage,
    YoutubelistPage,
    YoutubePipe,
    
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    IonicImageViewerModule,

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,ModalPage,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    ProjecthomePage,
    New7wonderPage,
    Mid7wonderPage,
    Old7wonderPage,
    YoutubelistPage,
  ],
  providers: [
    TabsService,
    StatusBar,
    SplashScreen,
    Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GoogleCloudVisionServiceProvider,
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: ionicGalleryModal.GalleryModalHammerConfig
    }
  ]
})
export class AppModule {}
