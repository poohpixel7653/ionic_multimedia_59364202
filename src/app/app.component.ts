import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { ProjecthomePage } from '../pages/projecthome/projecthome';
import { HomePage } from '../pages/home/home';
import { TabsService } from '../shared/Service/tabs.service';
import { YoutubelistPage } from '../pages/youtubelist/youtubelist';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = TabsPage;
  @ViewChild(Nav) nav : Nav;

  constructor(public tabsService : TabsService,platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  openPageHome() {
    this.nav.setRoot(ProjecthomePage);
  }

  backToIonicClass(){
    this.nav.setRoot(HomePage);
    this.tabsService.show();
  }

  openPageYoutube(){
    this.nav.setRoot(YoutubelistPage);
  }
}
