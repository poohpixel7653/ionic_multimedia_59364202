import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';

/**
 * Generated class for the InfoModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {
  value : any;
  //detail = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public ViewController : ViewController) {
    this.value = navParams.get('items');
    
  }

  ionViewDidLoad() {
    //this.detail  = JSON.parse(JSON.stringify(this.value))
    //console.log(this.value);
    //console.log('ionViewDidLoad InfoModalPage');
  }

  closeModal(){
    this.ViewController.dismiss();
  }

}