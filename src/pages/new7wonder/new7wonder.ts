import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the New7wonderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-new7wonder',
  templateUrl: 'new7wonder.html',
})


export class New7wonderPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

}
